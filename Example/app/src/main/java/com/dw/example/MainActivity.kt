package com.dw.example

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.dw.example.ui.theme.ExampleTheme
import com.smart.cc.stepcounter.PEDOMETERALG

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ExampleTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    DynamicTextExample()
                }
            }
        }
    }

}

@Composable
fun DynamicTextExample() {
    var textState by remember { mutableStateOf("Initial Text") }

    var fd = PEDOMETERALG.gsensorOpen()
    if (fd < 0) {
        Log.e("MainActivity", "open pedo error")
    }
    Column {
        Text(text = textState,
            color = Color.Blue,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentSize(Alignment.Center)
                .padding(40.dp)
                )
        Button(onClick = {
            var steps = PEDOMETERALG.getGsensorSteps()
            Log.d("MainActivity", "open pedo steps : " + steps)
            textState = steps.toString()
        }) {
            Text("Get Steps")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DynamicTextExamplePreview() {
    Column {
        Text(text = "12354",
            color = Color.Blue,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentSize(Alignment.Center)
                .padding(40.dp)
        )
        Button(onClick = {

        }) {
            Text("Get Steps")
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    ExampleTheme {
        Greeting("Android")
    }
}
