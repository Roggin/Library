package com.dw.test.keyboard.input;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.dw.test.keyboard.BuildConfig;
import com.dw.test.keyboard.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DebugCommandActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_command);
        TextView textView = (TextView) findViewById(R.id.textCommad);
        if (BuildConfig.DEBUG) {
            textView.setText(generalDynamicConfirmPassword());
        } else {
            String buildTime = getBuildTime();
            //Log.d("HJR","manifest:" + buildTime + ",buildConfig:" + BuildConfig.BUILD_TIME + ",resValue:" + getString(R.string.build_time));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date now = new Date();
            String nowTime = sdf.format(now);
            if (!nowTime.equals(buildTime) && !nowTime.equals(BuildConfig.BUILD_TIME)) {
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'", Locale.getDefault());
                textView.setText(sdf2.format(now));
            } else {
                textView.setText(generalDynamicConfirmPassword());
            }
        }
    }

    private String generalDynamicConfirmPassword() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int random = (day * month + 124) % 100;
        SimpleDateFormat format = new SimpleDateFormat("ddHH");
        String timeStr = String.format("%02d", random) + format.format(calendar.getTime());
        String password = reverseString(timeStr);
        return password;
    }


    public String reverseString(String original) {
        StringBuilder sb = new StringBuilder(original);
        return sb.reverse().toString();
    }

    public String getBuildTime() {
        try {
            ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            String buildTime = bundle.getString("build_time");
            // 使用buildTime作为你的需要
            return buildTime;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }
}
