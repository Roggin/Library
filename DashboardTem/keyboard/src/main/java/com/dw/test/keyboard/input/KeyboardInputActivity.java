package com.dw.test.keyboard.input;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import android.util.Log;

import com.dw.test.keyboard.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class KeyboardInputActivity extends Activity {
    private static final String[] KEY = new String[]{
            "1", "2", "3",
            "4", "5", "6",
            "7", "8", "9",
            "<<", "0", "Done"
    };
	
    private static final String DEFAULT_PASSWORD = "223399"; //tabindex:15
    private static final String DEFAULT_ERROR = "000000";
    
    private static final String DEFAULT_DEV_PASSWORD = "050901"; //tabindex:12
    
    private static final int TAB_DEVLOPMENT = 12;
    private static final int TAB_FACTORY = 15;
    
    private int mTabIndex = TAB_DEVLOPMENT;

    private PayEditText payEditText;
    private Keyboard mKeyboard;
    private boolean isInputFinish = false;

    @Override
    protected void onStop() {
        super.onStop();
        //exit for error
        if (!isInputFinish) {
        	finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); // 设置无标题栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_keyboard);
        initView();
        setSubView();
        initEvent();
        
        Intent intent = getIntent();
        if (intent != null) {
       		mTabIndex = intent.getIntExtra("tabindex",TAB_DEVLOPMENT);
        }
    }

    private void initView() {
        payEditText = (PayEditText) findViewById(R.id.PayEditText_pay);
        mKeyboard = (Keyboard) findViewById(R.id.KeyboardView_pay);
    }

    private void setSubView() {
        //设置键盘
        String[] keyboard = getResources().getStringArray(R.array.keyboard);
        mKeyboard.setKeyboardKeys(keyboard);
    }

    private void initEvent() {
        mKeyboard.setOnClickKeyboardListener(new Keyboard.OnClickKeyboardListener() {
            @Override
            public void onKeyClick(int position, String value) {
                if (position < 11 && position != 9) {
                    payEditText.add(value);
                } else if (position == 9) {
                    payEditText.remove();
                } else if (position == 11) {
                    onFinishInput(payEditText.getText());
                    //当点击完成的时候，也可以通过payEditText.getText()获取密码，此时不应该注册OnInputFinishedListener接口
                }
            }
        });

        /**
         * 当密码输入完成时的回调
         */
        payEditText.setOnInputFinishedListener(new PayEditText.OnInputFinishedListener() {
            @Override
            public void onInputFinished(String password) {
                //onFinishInput(password);
                Log.v("KeyboardInput","您的密码是：" + password);
            }
        });
    }

    private void onFinishInput(String password) {
        if (TextUtils.isEmpty(password)) {
            Log.e("KeyboardInput","input password is empty!");
            return ;
        }
        Log.v("KeyboardInput","onFinishInput password：" + password);
        checkPassword(mTabIndex,password);
        isInputFinish = true;
        finish();
    }
    
    private void checkPassword(int tabindex,String password) {
    	if (tabindex == TAB_FACTORY) {
    		if (DEFAULT_PASSWORD.equals(password)) {
	            Log.v("KeyboardInput","input password ok");
	            //Intent startIntent =new Intent();
                //startIntent.setClassName("com.android.settings",
                //                    "com.android.settings.DwEngModeMainAcitivity");
                //startActivity(startIntent);
	        } else {
	            Log.w("KeyboardInput","input password error:" + tabindex);
	        }
    	} else if (tabindex == TAB_DEVLOPMENT) {
    		String confirmPassword = generalDynamicConfirmPassword();
    		if (confirmPassword.equals(password) || DEFAULT_DEV_PASSWORD.equals(password)) {
	            Log.v("KeyboardInput","input password ok");
	            openDevelopSetActivity(this);
	        } else {
	            Log.w("KeyboardInput","input password error:" + tabindex);
	        }
    	} else {
    	
    	}
    }
    
    private String generalDynamicConfirmPassword() {
    	Calendar calendar = Calendar.getInstance();
    	int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int random = (day * month + 124) % 100;
		SimpleDateFormat format=new SimpleDateFormat("ddHH");
		String timeStr = String.format("%02d", random) + format.format(calendar.getTime());
		String password =  reverseString(timeStr);
		//Log.e("KeyboardInput","timeStr:"+timeStr);
		//Log.e("KeyboardInput","password:"+password);
		return password;
    }
    
    private void openDevelopSetActivity(Context context) {
    	//Intent intent = new Intent();
		//intent.setClass(context, DwDevelopmentSettingsActivity.class);
		//intent.putExtra(DwDevelopmentSettingsActivity.DEBUG_KEY, DwDevelopmentSettingsActivity.DEBUG_DEBUGGING_TYPE);
		//intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		//context.startActivity(intent);
    }
       
    public String reverseString(String original) {
        StringBuilder sb = new StringBuilder(original);
        return sb.reverse().toString();
    }
}
