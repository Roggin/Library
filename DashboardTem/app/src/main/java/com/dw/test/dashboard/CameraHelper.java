package com.dw.test.dashboard;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;

public class CameraHelper {
 
    public static int getCameraOrientation(int cameraId) {
        CameraInfo info = new CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = 0; // 默认方向
        // 根据设备方向和摄像头方向来设置角度
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            rotation = (info.orientation + rotation) % 360;
        } else { // back-facing camera
            rotation = (info.orientation - rotation + 360) % 360;
        }
        return rotation;
    }
}