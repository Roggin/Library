package com.dw.test.dashboard

import android.app.WallpaperManager
import android.content.ComponentName
import android.content.Intent
import android.content.res.Resources
import android.content.res.Resources.NotFoundException
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.util.Log
import android.view.WindowManager
import android.view.animation.Animation
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import java.io.IOException


class MainActivity : AppCompatActivity() {

    private lateinit var tm: TelephonyManager
    private var mPhoneStateListener: PhoneStateListener? = null

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // 获取 Button 对象
        val button = findViewById<Button>(R.id.button)
        // 设置点击事件监听器
        button.setOnClickListener { // 在点击事件处理方法中显示 Toast 消息
            //setWallpaper()
            //starWeatherService()
            /*tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            Log.d("HJR",tm.getNetworkOperatorName())

            if (mPhoneStateListener == null) {
                mPhoneStateListener = object : PhoneStateListener() {
                    override fun onServiceStateChanged(serviceState: ServiceState) {
                        // SPRD: [bug787914] Show network operator when service state in STATE_EMERGENCY_ONLY.
                        if (serviceState.state == ServiceState.STATE_IN_SERVICE) {
                            // SPRD: [bug717465] Translate operator name into current language.
                            val netOperatorName: String = tm.getNetworkOperatorName()
                            Log.d("HJR","netOperatorName:" + netOperatorName)
                            Log.d("HJR","netOperatorName2:" + updateOperator(netOperatorName, "operator"))
                        }
                    }
                }
            }
            tm.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SERVICE_STATE)*/
            Log.d("HJR","windowmanager:" + window.attributes.title)
            Log.d("HJR","activity:" + title)
            WindowManager.LayoutParams.FIRST_APPLICATION_WINDOW

            val cameraId = 0 // 假设有两个摄像头，选择第一个
            val cameraOrientation = CameraHelper.getCameraOrientation(cameraId)
            Log.d("HJR","cameraOrientation:" + cameraOrientation)
        }
    }

    fun updateOperator(value: String, arrayName: String): String? {
        val r = Resources.getSystem()
        var newName = value
        Log.d("HJR", " changeOperator: old value= $value")
        try {
            val identify = r.getIdentifier(arrayName, "array", "android")
            val itemList = r.getStringArray(identify)
            Log.d("HJR", " changeOperator: itemList length is " + itemList.size)
            for (item in itemList) {
                val parts = item.split("=".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()
                if (parts[0].equals(value, ignoreCase = true)) {
                    newName = parts[1]
                    Log.d(
                        "HJR", "itemList found: parts[0]= " + parts[0] +
                                " parts[1]= " + parts[1] + "  newName= " + newName
                    )
                    return newName
                }
            }
        } catch (e: NotFoundException) {
            Log.e("HJR", "Error, string array resource ID not found: $arrayName")
        }
        Log.d(
            "HJR",
            "changeOperator not found: original value= $value newName= $newName"
        )
        return newName
    }

    override fun onPause() {
        super.onPause()
        /*if (mPhoneStateListener != null) {
            tm.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
        }*/
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun starWeatherService() {
        /*val componentName =
            ComponentName(
                "com.diwo.weather",
                "com.diwo.weather.WeatherService"
            ) // 定时通知天气app 更新天气
        val intent = Intent()
        intent.component = componentName
        startForegroundService(intent) // 定时需要天气app更新数据*/
        /*var jobScheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        var builder:JobInfo.Builder  = JobInfo.Builder(113, ComponentName("com.diwo.weather","com.diwo.weather.WeatherService"));
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY) // 设置网络连接条件
        jobScheduler.schedule(builder.build())*/
        // 使用PackageManager查找Service
        // 使用PackageManager查找Service
        val intent = Intent()
        intent.action = "android.intent.action.weather"
        val packageManager = packageManager
        val resolveInfo = packageManager.resolveService(intent, 0)

        if (resolveInfo != null) {
            // 找到了Service，创建ComponentName
            val componentName = ComponentName(
                resolveInfo.serviceInfo.applicationInfo.packageName,
                resolveInfo.serviceInfo.name
            )
            // 创建带有ComponentName的Intent
            intent.component = componentName
            // 启动Service
            startForegroundService(intent)
        } else {
            // 未找到Service
            Log.e("MainActivity", "Service not found")
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun setWallpaper() {
        // 获取 WallpaperManager 的实例
        val wallpaperManager = WallpaperManager.getInstance(applicationContext)
        // 从资源中获取 Bitmap
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.lockscreen_wallpaper)
        try {
            // 设置锁屏壁纸
            wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_LOCK)
            Toast.makeText(
                applicationContext,
                "Lock screen wallpaper set successfully",
                Toast.LENGTH_SHORT
            ).show()
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(
                applicationContext,
                "Failed to set lock screen wallpaper",
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}