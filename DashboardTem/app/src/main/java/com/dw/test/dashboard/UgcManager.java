package com.dw.test.dashboard;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class UgcManager {

    private static final String DOUYIN_PACKAGE_NAME = "com.ss.android.ugc.aweme"; // 抖音应用的包名
    private static final String WEXIN_PACKAGE_NAME = "com.tencent.mm"; // 微信应用的包名
    private static final long USAGE_TIME_THRESHOLD = 10 * 60 * 1000; // 1小时
    private Timer timer;
    private long startTime;
    private long totalTimeUsed;
    private boolean isDouyinRunning;

    private Context mContext;
    public UgcManager(Context context) {
        mContext = context;
    }

    public void start() {
        // 启动定时器
        startTimer();
    }

    private void startTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                checkDouyinRunning();
                if (isDouyinRunning) {
                    updateTotalTimeUsed();
                    if (totalTimeUsed >= USAGE_TIME_THRESHOLD) {
                        // 超过使用时长阈值，显示提示消息
                        showExitPrompt();
                    }
                } else {
                    // 应用未运行时重置计时器
                    long currentTime = System.currentTimeMillis();
                    if (currentTime - startTime > 30 * 1000) {
                        resetTimer();
                    }
                }
            }
        }, 0, 1000); // 每秒查询一次
    }

    private void checkDouyinRunning() {
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.processName.equals(DOUYIN_PACKAGE_NAME)) {
                isDouyinRunning = true;
                break;
            } else {
                isDouyinRunning = false;
            }
        }
    }

    public static boolean isDouyinForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (activityManager != null) {
            // 获取当前正在运行的应用程序信息
            ComponentName componentName = activityManager.getRunningTasks(1).get(0).topActivity;
            // 检查包名是否为抖音的包名
            return componentName.getPackageName().equals("com.ss.android.ugc.aweme");
        }
        return false;
    }

    private void updateTotalTimeUsed() {
        long currentTime = System.currentTimeMillis();
        totalTimeUsed += (currentTime - startTime);
        startTime = currentTime;
    }

    private void resetTimer() {
        startTime = System.currentTimeMillis();
        totalTimeUsed = 0;
    }

    private void showExitPrompt() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, "您已连续使用抖音超过1小时，请适当休息并退出应用！", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void stop() {
        if (timer != null) {
            timer.cancel();
        }
    }
}